\documentclass[aip,jcp,reprint,noshowkeys,superscriptaddress]{revtex4-1}

\usepackage{graphicx,dcolumn,bm,xcolor,hyperref,multirow,amsmath,amssymb,amsfonts,physics}
\usepackage[normalem]{ulem}
\usepackage[version=4]{mhchem}
\usepackage[compat=1.1.0]{tikz-feynman}
\usepackage[tracking]{microtype}
\usepackage{mathpazo,libertine}

\usepackage{algorithmicx,algorithm,algpseudocode}
\algnewcommand\algorithmicassert{\texttt{assert}}
\algnewcommand\Assert[1]{\State \algorithmicassert(#1)}
\algrenewcommand{\algorithmiccomment}[1]{$\triangleright$ #1}


\newcommand{\alert}[1]{\textcolor{red}{#1}}
\definecolor{darkgreen}{RGB}{0, 180, 0}

%\usepackage{hyperref}

\newcommand{\cdash}{\multicolumn{1}{c}{---}}
\newcommand{\eps}{\epsilon}
\newcommand{\mc}{\multicolumn}
\newcommand{\mr}{\multirow}
\newcommand{\fnm}{\footnotemark}
\newcommand{\fnt}{\footnotetext}
\newcommand{\mcc}[1]{\multicolumn{1}{c}{#1}}

% functionals, potentials, densities, etc
\newcommand{\e}[2]{\eps_\text{#1}^{#2}}
\newcommand{\n}[1]{n^{#1}}
\newcommand{\DD}[2]{\Delta_\text{#1}^{#2}}
\newcommand{\LZ}[2]{\Xi_\text{#1}^{#2}}

	
% energies
\newcommand{\EHF}{E_\text{HF}}
\newcommand{\Ec}{E_\text{c}}
\newcommand{\Ecat}{E_\text{cat}}
\newcommand{\Eneu}{E_\text{neu}}
\newcommand{\Eani}{E_\text{ani}}
\newcommand{\EPT}{E_\text{PT2}}
\newcommand{\EFCI}{E_\text{FCI}}
\newcommand{\br}{\mathbf{r}}
\newcommand{\bw}{\mathbf{w}}
\newcommand{\bP}[1]{\mathbf{P}^{#1}}
\newcommand{\bG}{\mathbf{G}}
\newcommand{\bH}{\mathbf{H}}
\newcommand{\bF}[1]{\mathbf{F}^{#1}}

% units
\newcommand{\IneV}[1]{#1 eV}
\newcommand{\InAU}[1]{#1 a.u.}
\newcommand{\InAA}[1]{#1 \AA}

\newcommand{\LCPQ}{Laboratoire de Chimie et Physique Quantiques (UMR 5626), Universit\'e de Toulouse, CNRS, UPS, France}
\newcommand{\LCQ}{Laboratoire de Chimie Quantique, Institut de Chimie, CNRS, Universit\'e de Strasbourg, Strasbourg, France}

%%% Manu %%%
\definecolor{dgreen}{rgb}{0,.5,0}
\newcommand{\manu}[1]{{\textcolor{dgreen}{ Manu: #1 }} }
\newcommand{\be}{\begin{eqnarray}}
\newcommand{\ee}{\end{eqnarray}}

\begin{document}	

\title{Notes on density-functional theory for ensembles}

\author{Pierre-Fran\c{c}ois Loos}
\email{loos@irsamc.ups-tlse.fr}
\affiliation{\LCPQ}

\begin{abstract}
\end{abstract}

\maketitle

%%%%%%%%%%%%%%%%%%%%%
\section{Optical gap}
%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%
\subsection{The weight-dependent functional}
%%%%%%%%%%%%%%%%%%%%%
The present weight-dependent density-functional approximation for ensemble (eDFA) is specifically designed for the calculation of excitation energies within eDFT.
In order to take into bot single and double excitations simultaneously, the present eDFA is based on the ground state ($I=0$), first singly-excited state ($I=1$) and first doubly-excited states ($I=2$) of the two-electron ringium system.
This functional can be seen as a local-density approximation (LDA) type functional for ensembles.
All these states have the same (uniform) density $n = 2/(2\pi R)$ where $R$ is the radius of the ring where the electrons are confined.

One can easily write down the reduced (i.e.~per electron) Hartree+exchange (Hx) energy of these three states:
\begin{align}
	\e{Hx}{(0)}(n) & = n,
	&	 
	\e{Hx}{(1)}(n) & = 4 n/3,		
	&	 
	\e{Hx}{(2)}(n) & = 23 n/15.	
\end{align}
Similarly, we can provide an accurate analytical expression of the correlation (c) energy via the following Pad\'e approximant
\begin{equation}
	\e{c}{(I)}(n) = \frac{a^{(I)}\,n}{n + b^{(I)} \sqrt{n} + c^{(I)}}
\end{equation}
where $b^{(I)}$ and $c^{(I)}$ are state-dependent fitting parameters, which are provided in Table \ref{tab:OG_func}.
$a^{(I)}$ is obtained via the high--density expansions of the correlation energy.

%%% TABLE 1 %%%
\begin{table*}
	\caption{\label{tab:OG_func}
	Parameters of the correlation functional for the optical gap.}
	\begin{ruledtabular}
		\begin{tabular}{lcddd}
			State						&	$I$		&	\mcc{$a^{(I)}$}		&	\mcc{$b^{(I)}$}		&	\mcc{$c^{(I)}$}	\\
			\hline
			Ground state				&	$0$		&	-0.0137078			&	0.0748000			&	0.0566294		\\
			First singly-excited state	&	$1$		&	-0.0240765			&	0.0553987			&	0.0163852		\\
			First doubly-excited state	&	$2$		&	-0.00935749			&	0.0334572			&	-0.0249377		\\
		\end{tabular}
	\end{ruledtabular}
\end{table*}

Based on these quantities, one can build three-state weight-dependent functionals:
\begin{subequations}
\begin{align}
	\e{Hx}{\bw}(n) & =  (1-w_1-w_2) \e{Hx}{(0)}(n) + w_1 \e{Hx}{(1)}(n) + w_2 \e{Hx}{(2)}(n),
	\\
	\e{c}{\bw}(n) & =  (1-w_1-w_2) \e{c}{(0)}(n) + w_1 \e{c}{(1)}(n) + w_2 \e{c}{(2)}(n)
	\\
	\e{Hxc}{\bw}(n) & =  \e{Hx}{\bw}(n) + \e{c}{\bw}(n),
\end{align}
\end{subequations}
with $\bw = (w_1,w_2)$.
%For notational convenience, we also define the corresponding potentials as
%\begin{align}
%	\v{Hx}{\bw}(n) & = n\,\e{Hx}{\bw}(n)
%	&
%	\v{c}{\bw}(n) & = n\,\e{c}{\bw}(n)
%	&
%	\v{Hxc}{\bw}(n) & = n\,\e{Hxc}{\bw}(n)
%\end{align}

In the "c-only" case, the individual energies can be extracted as follows
\begin{equation}
\begin{split}
	E^{(I)} & = \frac{1}{2}  \Tr[\bP{(I)} \cdot (\bH + \bF{(I)})] 
			\\
			& + \int \qty{ \e{c}{\bw}[\n{\bw}(\br)] + \n{\bw}(\br) \left. \fdv{\e{c}{\bw}[\n{}]}{\n{}(\br)} \right|_{\n{} = \n{\bw}(\br)} } \n{(I)}(\br) d\br
			\\
			& + \LZ{c}{} + \DD{c}{(I)}.
\end{split}
\end{equation}
where $\bH$ is the core Hamiltonian, $\bF{(I)}$ and $\bP{(I)}$ are the Fock and the density matrix associated with the $I$th state and $\n{(I)}(\br)$ its corresponding density.
The correlation part of thr (state-independent) Levy-Zahariev shift and the so-called derivative discontinuity are given by
\begin{align}
	\LZ{c}{} 	& =  - \int \left. \fdv{\e{c}{\bw}[\n{}]}{\n{}(\br)} \right|_{\n{} = \n{\bw}(\br)} \n{\bw}(\br)^2 d\br,
	\\
	\DD{c}{(I)} & = \sum_{J>0} (\delta_{IJ} - w_J) \int  \left. \pdv{\e{c}{\bw}[\n{}]}{w_J}\right|_{\n{} = \n{\bw}(\br)} \n{\bw}(\br) d\br
\end{align}
where we note that, by construction,
\begin{equation}
	\left. \pdv{\e{c}{\bw}[\n{}]}{w_J}\right|_{\n{} = \n{\bw}(\br)} = \e{c}{(J)}[\n{\bw}(\br)] - \e{c}{(0)}[\n{\bw}(\br)] 
\end{equation}

In the case of a "Hxc" functional, we have
\begin{equation}
\begin{split}
	E^{(I)} & = \Tr[\bP{(I)} \cdot \bH] 
			\\
			& + \int \qty{ \e{Hxc}{\bw}[\n{\bw}(\br)] + \n{\bw}(\br) \left. \fdv{\e{Hxc}{\bw}[\n{}]}{\n{}(\br)} \right|_{\n{} = \n{\bw}(\br)} } \n{(I)}(\br) d\br
			\\
			& + \LZ{Hxc}{} + \DD{Hxc}{(I)}.
\end{split}
\end{equation}
with
\begin{align}
	\LZ{Hxc}{} 	 	& =  - \int \left. \fdv{\e{Hxc}{\bw}[\n{}]}{\n{}(\br)} \right|_{\n{} = \n{\bw}(\br)} \n{\bw}(\br)^2 d\br,
	\\
	\DD{Hxc}{(I)} 	& = \sum_{J>0} (\delta_{IJ} - w_J) \int  \left. \pdv{\e{Hxc}{\bw}[\n{}]}{w_J}\right|_{\n{} = \n{\bw}(\br)} \n{\bw}(\br) d\br
\end{align}

%%%%%%%%%%%%%%%%%%%%%
\subsection{Implementation}
%%%%%%%%%%%%%%%%%%%%%

%%% FIGURE 1 %%%
\begin{figure}
	\begin{algorithmic}[1]
		\Procedure{Optical Gap}{}
			\State 
			\While{}
				\State 
				\State 
%				\For{}
%					\State 
%				\EndFor
			\EndWhile 
%			\If{}
%				\State 
%			\EndIf
		\EndProcedure
	\end{algorithmic}
	\caption{
		\label{fig:algo_OG}
		Pseudo-code for the calculation of the optical gap within eDFT.
		$\tau$ is a user-defined threshold.
	}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%
\section{Fundamental gap}
%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%
\subsection{The weight-dependent functional}
%%%%%%%%%%%%%%%%%%%%%

Similarly to what have been done for the optical gap, one can design easily designed a weight-dependent correlation eDFA for the fundamental gap.
This time, our functional is based on the neutral ($I=0$), cation ($I=1$) and anion ($I=2$) species.
These systems contain respectively, one, two and three electrons.
Contrary to the optical gap, these three states have different densities which read
\begin{align}
	\n{(0)} & = 2/(2\pi R),		
	&
	\n{(1)} & = 1/(2\pi R),
	&	 
	\n{(2)} & = 3/(2\pi R).	
\end{align}
Their reduced Hartree+exchange (Hx) energy are given by
\begin{align}
	\e{Hx}{(0)}(n) & = n,		
	&	 
	\e{Hx}{(1)}(n) & = 0,
	&	 
	\e{Hx}{(2)}(n) & = 40 n/27.	
\end{align}
The expression of the correlation functional is identical to the previous case
\begin{equation}
	\e{c}{(2)}(n) = \frac{a^{(2)}\,n}{n + b^{(2)} \sqrt{n} + c^{(2)}}
\end{equation}
where the coefficients are reported in Table \ref{tab:OG_func}.
We note that $\e{c}{(0)}$ has been defined in the previous section and the one-electron cationic system has $\e{c}{(1)}(n) = 0$.

%%% TABLE 2 %%%
\begin{table*}
	\caption{\label{tab:OG_func}
	Parameters of the correlation functional for the optical gap.}
	\begin{ruledtabular}
		\begin{tabular}{lcddd}
			State		&	$I$	&	\mcc{$a^{(I)}$}		&	\mcc{$b^{(I)}$}		&	\mcc{$c^{(I)}$}	\\
			\hline
			Neutral		&	0		&	-0.0137078			&	0.0748000			&	0.0566294		\\
			Cation		&	1		&	0					&	0				&	0		\\
			Anion		&	2		&	-0.0184842			&	0.0896216			&	0.0175732		\\
		\end{tabular}
	\end{ruledtabular}
\end{table*}


Because of their difference in density, one needs to accommodate the weights in order to create a density-fixed eDFA.
We have chosen the following form:
\begin{equation}
	\e{c}{\bw}(n) = \qty(1 - \frac{N-1}{N} w_1 - \frac{N+1}{N} w_2) \e{c}{(0)} + w_1 \e{c}{(1)} + w_2 \e{c}{(2)}
\end{equation} 
with, in our case, $N=2$.



In the case of "c" functional, the indivdual energies are given by
\begin{equation}
\begin{split}
	E^{(I)} & = \frac{1}{2} \Tr[\bP{(I)} \cdot (\bH+\bF{(I)})] 
			\\
			& + \int \qty{ \e{c}{\bw}[\n{\bw}(\br)] + \n{\bw}(\br) \left. \fdv{\e{c}{\bw}[\n{}]}{\n{}(\br)} \right|_{\n{} = \n{\bw}(\br)} } \n{(I)}(\br) d\br
			\\
			& + \LZ{c}{(I)} + \DD{c}{(I)}.
\end{split}
\end{equation}
where we note that, now, the Levy-Zahariev shift is state dependent.

\begin{align}
	\LZ{c}{(I)} 	& =  - \int \left. \fdv{\e{c}{\bw}[\n{}]}{\n{}(\br)} \right|_{\n{} = \n{\bw}(\br)} \n{\bw}(\br)^2 d\br,
	\\
	\DD{c}{(I)} & = \sum_{J>0} (\delta_{IJ} - w_J) \int  \left. \pdv{\e{c}{\bw}[\n{}]}{w_J}\right|_{\n{} = \n{\bw}(\br)} \n{\bw}(\br) d\br
\end{align}

In the case of a "Hxc" functional for the fundamental gap, we have similar expressions as for the optical gap.




%%% FIGURE 2 %%%
\begin{figure}
	\begin{algorithmic}[1]
		\Procedure{Fundamental Gap}{}
			\State 
			\While{}
				\State 
				\State 
%				\For{}
%					\State 
%				\EndFor
			\EndWhile 
%			\If{}
%				\State 
%			\EndIf
		\EndProcedure
	\end{algorithmic}
	\caption{
		\label{fig:algo_FG}
		Pseudo-code for the calculation of the fundamental gap within eDFT.
		$\tau$ is a user-defined threshold.
	}
\end{figure}

\end{document}
