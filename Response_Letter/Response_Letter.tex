\documentclass[10pt]{letter}
\usepackage{UPS_letterhead,xcolor,mhchem,mathpazo,ragged2e,hyperref,physics,amsmath}
\newcommand{\alert}[1]{\textcolor{red}{#1}}
\definecolor{darkgreen}{HTML}{009900}
\newcommand{\manu}[1]{\textcolor{blue}{Manu: #1}}


\begin{document}

\begin{letter}%
{To the Editors of the Journal of Chemical Physics}

\opening{Dear Editors,}

\justifying
Please find attached a revised version of the manuscript entitled 
\begin{quote}
\textit{``A weight-dependent local correlation density-functional approximation for ensembles''}.
\end{quote}
We thank the reviewers for their constructive comments.
Our detailed responses to their comments can be found below.
For convenience, changes are highlighted in red in the revised version of the manuscript. 

We look forward to hearing from you.

\closing{Sincerely, the authors.}

%%% REVIEWER 2 %%%
\noindent \textbf{\large Authors' answer to Reviewer \#2}

\begin{itemize}

	\item 
	{This is an excellent paper of great importance to the growing ensemble DFT community, as well as the wider formal DFT community. 
	It outlines a new method for generating an ensemble-weight-dependent correlation approximation based on uniform electron gas model systems, called the eLDA. 
	This approach to generating a local density functional approximation to the correlation energy uses an expansion of the correlation energy functional around the Kohn-Sham state density for the state whose excitation energy is of interest. 
	The authors embed this expression of approximate ensemble effects for a two-electron finite uniform electron gas inside an infinite uniform electron gas, in order to build upon the traditional, ground-state LDA and make their approach applicable to many-electron systems. 
	This is explicitly connected to previous work on the generalized adiabatic connection for ensembles and the ghost interaction correction, which makes clear how the authors relate exact correlation ensemble effects to the contribution from 2-electron LDA correlation differences. 
	The approach is demonstrated with a clever use of one-dimensional boxes that allow investigation of its behavior in different correlation-strength regimes. 

	The paper is significant, well written, and thoughtful. 
	I recommend its publication, though I do have some suggestions for improving its accessibility to a wider audience. 
	I also have some questions, answers to which might improve the work's interest and utility for formal DFT investigators, if the answers are readily available. }
	\\
	\alert{We thank the reviewer for his/her kind comments.}

	\item 
	{Page 4: barred correlation per particle is introduced implicitly in Eqn 41. 
	Some pieces of the later discussion of this quantity might be helpful here, since this quantity appears in discussion of eqns 48 and 49, }
	\\
	\alert{For clarity, we now mention that the expression in Eq. (41)
has a general structure, which should be exact for any uniform system. We
point out that, unlike in the exact theory, the correlation components
of eLDA are weight-independent. We
also mention explicitly that these components will be constructed (in
the following) from
the correlation excitation energies of a finite uniform electron gas. We
refer to Sec. III for further details.}

	\item 
	{Page 4: It would be helpful to note clearly near eqn 40 that the authors' focus on LDA correlation differs from what many readers will be used to in ground-state LDA, namely using LDA exchange as well, instead of combining it with HF Hartree-exchange as is done here. 
	The combination of exchange and correlation leads to some well-known behavior that readers should not rely upon when reading this work, so it would be a helpful gesture to remind them. 
}
	\\
	\alert{The reviewer is right. 
	We have added a note to clarify this point after Eq.~(41).}

	\item 
	{Page 5: The authors' expansion of the correlation energy around the $I$th state and its resulting neglect of correlation effects between states more remote from one another might affect evaluation and analysis of the approximation and/or the embedding scheme. 
	Could the authors note around eqn 47 somewhere how they decided this expansion was valid and useful, as well as how they anticipate this approximation might affect their later evaluation and analysis of the approximation, and similar for the embedding scheme? 
	This is mentioned elsewhere in the text, but treating it here would bolster the authors' narrative and support their choices more. }
	\\
	\alert{That is a good point. In order to motivate the derivation
of these Taylor expansions more clearly, we now mention in the revised
manuscript that the individual eLDA correlation energy expressions do
not give much insight as purely individual and mixed terms cannot be
readily distinguished. This is exactly what the Taylor expansions enable
us to do. We also mention that these expansions become useful when
analyzing the performance of eLDA (we refer to the discussion section).
Most importantly, unlike in the original manuscript, we explain the
relevance of these
expansions by considering weak deviations from the uniform density
regime. Indeed, in this case, eLDA is a reasonable approximation and the
difference in density between the ensemble and the individual states is
small. Let us finally stress that our embedding strategy does not rely on these
Taylor expansions. They are exclusively used for analysis purposes in
this work. As written explicitly in the revised manuscript, they just
give more insight into eLDA.}

	\item 
	{Page 5: Though the ringium model is developed elsewhere in the literature in great detail, a diagram for readers not as familiar with it would be a kindness. }
	\\
	\alert{Following the reviewer's suggestion, we have added a figure to illustrate the ringium model in the supporting information.}

	\item 
	{Page 6: How well does equation 53 work in the low-density limit, or in the regime approaching the low-density limit? 
	Their previous work has noted elsewhere the logarithmic behavior of correlation for this model in that regime. 
	Is it important here? 
	To mimic parametrizations used in ground-state LDA, both pieces of correlation behavior would be captured, so readers may question this as I do. }
	\\
	
	\alert{By construction, Eq.~(53) is exact in the high-density limit as the coefficient $a_1^{(I)}$ is set to do so.
	This point is mentioned in the original manuscript.
	In the low-density (i.e., large $R$) regime, Eq.~(53) behaves as 
	\begin{equation}
		\varepsilon_c^{(I)} = \frac{a_1^{(I)}}{a_3^{(I)} \pi} \frac{1}{R} - \frac{a_1^{(I)} a_2^{(I)}}{(a_2^{(I)})^2 \pi^{3/2}} \frac{1}{R^{3/2}} + \order{R^{-2}}
	\end{equation}
	which is the correct behavior in terms of $R$, the first term (proportional to $R^{-1}$) representing the classical Coulomb repulsion between electrons and the second term (proportional to $R^{-3/2}$) representing the zero-point vibrational energy of the electrons.
	This is the usual Wigner crystal representation as mentionned in our previous works.
	The asymptotic behavior is then correct in the large-$R$ limit.
	However, the coefficients do not exactly match the exact ones.
	From a numerical and practical point of view, we have not found that enforcing the exact values does improve the results.
	Actually, it usually worsen them as enforcing the correct coefficients in the large-$R$ limit usually deteriorates the results for intermediate $R$ values.
	Finally, let us mention that the logarithmic behavior does not occur for the correlation energy.
	It only occurs in the thermodynamic limit (where the number of electrons gets very large) in the Hartree-exchange term.
	In any case, for small, finite number of electrons, there is no logarithmic behavior.
	We have added a small paragraph in the supporting information to further explain the behavior of Eq.~(53) in the low-density limit.}

	\item 
	{Page 6: Does the embedding scheme mean that the weight-dependence of the two-electron finite uniform system would ideally capture the weight-dependence for the infinite system? 
	And then subsequently that such an effect is being neglected by expressing this as the correlation for the two-electron ring system only? 
	Or is that not the correct way to think about this way of embedding? 
	I wonder if an explanatory diagram for the embedding scheme might clarify my thinking here, particularly if my thinking is wrong! }
	\\
	\alert{As mentioned in the original manuscript, the impurity carries the weight dependence of the functional.
	Performing a simple gedanken experiment, one can imagine that, in the infinite system, the excitation occurs locally, i.e., on the impurity.
	Therefore, we can assume, as a first approximation, that the weight dependence will originate mainly from this impurity, most of the bath being unaffected by this local excitation. This is, roughly speaking, the philosophy that we have followed.
	We believe this is also the reviewer's way of thinking.
	We have added a figure to illustrate this in the revised version of the manuscript.}

	\item 
	{Page 7: Another diagram suggestion: unfamiliar readers might be helped by a pedagogical diagram of your box systems, to help folks see how the different box lengths correspond to different correlation-strength regimes. }
	\\
	\alert{Very good suggestion. Accordingly, we have added a figure showing the electron density for a very small box and a very large box.
	This illustrates how electrons localize when the density gets smaller, and how the density has the tendency to be more uniform for small boxes.}

	\item 
	{Page 7: Does strong correlation always result in non-linear ghosts uncorrected by the GIC-eLDA, or is it particularly difficult or changed by the embedding scheme somehow? 
	Is this result influenced by the state mixing shown by FCI in Figure 3's discussion? }
	\\
	\alert{As now discussed in more detail in the revised manuscript,
the remaining non-linear ghosts originate from the weight dependence
(i.e. the dependence on the state mixing) of
the individual correlation energies, which is itself connected to the eLDA correlation
functional. This can be understood from the general ensemble correlation
energy per particle expression in Eq. (41) and the above-mentioned
Taylor expansions, without referring to our
embedding approach. In summary, the deviation from linearity of the
ensemble GIC-eLDA energy is a
general feature of ensemble LDA-type functionals.}

	\item 
	{Page 7: In the penultimate paragraph on this page, the discussion of Eqns 47 and 49 and the variation in the ensemble weights touches on one of the more subtle results of the GIC-eLDA, in my opinion, so it would be best to more explicitly describe this and its tie to the aforementioned equations.}
	\\
	\alert{For clarity, the discussion (which, indeed, consisted of a single
and brief sentence in the original manuscript) has been substantially
extended. In order to rationalize the observed linear or quadratic
variations of the individual GIC-eLDA energies, we first refer to Eqns 47
and 49 and mention explicitly that we use these expressions (which
are valid around the uniform density regime)
for {\it analysis} purposes only. The subsequent analysis, where we
highlight the terms that are
responsible for both linear and quadratic variations, is in perfect
agreement with our observations, thus illustrating the relevance and
usefulness of the Taylor expansions.}

	\item 
	{Fig. 1: It would be nice to see plots of these exact quantities for comparison, since there are layers of approximation and assumptions here. 
	Either that, or some similar demonstrations for the models used to build GIC-eLDA. }
	\\
	\alert{For clarity, the discussion of Fig. 1 (Fig. 3 in the
revised manuscript) has been extended. We now refer explicitly to the
expression of the GIC-eLDA ensemble energy where it can be readily seen
that its curvature can only originate from the weight-dependence of the
individual KS-eLDA energies. We then refer to the next paragraph where
we (now) explain where the linear and quadratic variations of the
individual energies come from (see our response to the previous comment).
The additional ghost-interaction errors that might be
introduced into the orbitals is then mentioned, as a second layer of
approximation. We also point out in the revised discussion that, in the exact theory,
individual energies would not exhibit any weight dependence, which means
that the deviation from linearity of the ensemble energy would be zero.}

	\item 
	{Fig. 2: Why does the crossover point for the 1st excitation curves disappear for $L=8\pi$? }
	\\
	\alert{The legend of Fig.~2 was incorrect (the curves were mislabeled), but this has now been corrected.
	In the corrected Fig.~2 (which is now Fig.~4 in the revised
manuscript), the crossover point occurs for two different states in  
two different ensembles. In other words, this point is not
interesting anymore. The discussion of this Figure has become much more fluid:
	when the weight of a state increases, this state is stabilized
while the two others increase in energy (as it should). 
	The discussion regarding this figure has been modified accordingly.}
%	\alert{It is clear from our derivations that the individual
%correlation energies should vary with both the density {\it
%and} the ensemble weights. There is in principle no reason to expect the
%same variations for different ensembles and density regimes. The fact
%that, for $L=8\pi$, electron correlation is strong and therefore the
%density is more localized, is probably the reason for the disappearance
%of the crossover point. We were not able to rationalize this observation
%further but we still mention in the revised manuscript that it is an
%illustration of the importance of both the density and the weights in
%the evaluation of individual energies within an ensemble.}

	\item 
	{Page 8: If the authors have evidence of behavior between $w=(0,0)$ and the equiensemble, instead of just these endpoints, that would be interesting to mention for the eDFT crowd. }
	\\
	\alert{Actually, we were probably not clear enough about what we
plotted in Figs. 1 and 2 of the original manuscript, but it was exactly
the continuous variation of (individual or ensemble) energies from
$w=(0,0)$ up to the equiensemble case $w=(1/3,1/3)$. For convenience,
the path was just split in two parts: a first one where $w_2=0$ and
$0\leq w_1\leq 1/3$, and a second one where $w_1=1/3$ and $0\leq w_2\leq
1/3$. For clarity, this is now mentioned explicitly in the revised
manuscrit before commenting on the plots.}

	\item 
	{Are there similar issues with combining HF exchange with LDA C as seen in the ground-state? 
	If not, why not? }
	\\
	\alert{
Interestingly, increasing the ensemble weights (which of course cannot
be done in conventional ground-state DFT calculations) leads to a
significant improvement of the ground-state energy while either
improving or not deteriorating too much the excited-state energies, at
least in the weak correlation regime. The main reason is the
destabilization of the ground state that occurs when increasing the
weights assigned to the excited states. This major difference between
practical 
conventional DFT (where the correlation energy would be overestimated)
and GOK-DFT calculations is a promising result that is
now highlighted in the revised manuscript.}
%\manu{We need to check the tables in the SI}

	\item 
	{Figure 3 discussion: Will eLDA always overestimate double excitations? 
	It's hard to see differences for small $L$. }
	\\
	\alert{Except for the two-electron system where we observed cases of underestimation, eLDA usually overestimates double excitations.
	The corresponding numerical data are reported in Supporting information.
	We have mentioned this interesting observation in the revised manuscript.}

	\item 
	{Clear statement that $w=(0,0)$ is the conventional ground-state HF X+LDA C KS orbital energy difference result should come earlier in the manuscript, to guide less familiar readers. 
	It currently occurs on page 9. }
	\\
	\alert{This is now mentioned earlier in the manuscript (see Computational Details section).}

	\item 
	{Page 9: Is the negligible effect of the second term of Eqn 51 on the excitation energies due to the focus on uniform systems or verified via FCI on boxium? }
	\\
	\alert{As mentioned in the original manuscript (see Results and Discussion section), we believe that it might be a consequence of how we constructed the eLDA functional, as the weight dependence of the eLDA functional is based on a two-electron uniform electron gas.
	We do not think this is due to the uniformity of its density, though.
	Incorporating a $N$-dependence in the functional through the curvature of the Fermi hole might be valuable in this respect. 
	This is left for future work.
	Besides, the difference in density between the ground and the excited states is not substantial in 1D systems, which makes the effect of the second term of Eqn 51 quite small.}

	\item 
	{Figure 6: Do the ground-state and equiensemble results for doubles converge as $N$ goes to infinity?}
	\\
	\alert{This is an excellent question. Currently, we do not have the data to check this fact as FCI calculations cannot be easily performed for larger number of electrons.
	Moreover, GOK-DFT calculations for larger number of electrons would require a larger one-electron basis set to be converged.
	Currently, our one-electron basis set is fixed to 30 basis functions.
	We hope to be able to answer this question in the near future.}

	\item 
	{Figure 6: Why is $N=7$ the number of particles where the DeltaC influence vanishes? 
	Does it remain negligible for higher particle numbers? }
	\\
	\alert{Currently, we do not have any explanation on why for $N=7$ the DeltaC influence vanishes.
	Our guess is that it might change sign but we could not compute the FCI energies for more than 7 electrons.
	Therefore, we cannot fully answer this question here.}
	
\end{itemize}

 
\end{letter}
\end{document}






