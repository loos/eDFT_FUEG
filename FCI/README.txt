On 19 Dec 2013, Peter Knowles sent me various files to allow me to do Full CI calculations.

To compile, I needed to do:

	gfortran -lblas fci.f standard.f

To run, I needed to do:

	./a.out < hf.dat

The resulting output compared well with the file hf.out that Peter K provided.

The program uses single-bar, not double-bar, integrals.
